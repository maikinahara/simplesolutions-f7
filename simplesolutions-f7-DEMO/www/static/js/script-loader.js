var splash,
  mainSpan,
  file1,
  file2,
  file3,
  file4,
  script1,
  script2,
  script3,
  loadedFiles = 0;

function initCordovaEvt () {
  // window.onerror = function myErrorHandler(errorMsg, url, lineNumber) {
  //   alert("Error occured: " + errorMsg);
  //   return false;
  // }
  console.log('deviceready add event')
  // document.addEventListener("deviceready", addScripts, false);
  checkConn()
}

function checkConn() {
  console.log('chk')
  // navigator.splashscreen.hide()
  var timeStamp = new Date().valueOf();
  var url = "https://simplesolutions.com.ar/site/app/blackie.png?x=" + timeStamp
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.timeout = 3000;
  xmlhttp.onload = function() {
    getScripts()
  }
  xmlhttp.onerror = function() {
    let el = document.getElementsByClassName('no-internet')[0]
    try {
      // navigator.splashscreen.hide()
      el.style.display = 'block';
      // navigator.notification.alert('Lo sentimos, debe tener conexión a internet para poder usar la aplicación',
      // function () {
      //   navigator.app.exitApp()
      // }, 'Aviso')
      document.getElementsByClassName('splash')[0].style.display = 'none';
    } catch (error)
    {
      el.style.display = 'block';
      document.getElementsByClassName('splash')[0].style.display = 'none';
      // alert('Lo sentimos, debe tenero conexión a internet para poder usar la aplicación')
      console.log(error)
    }
  }
  xmlhttp.open("GET", url, true);
  xmlhttp.send();
}
function getScripts() {
  splash = document.getElementsByClassName('splash')[0]
  mainSpan = document.getElementsByClassName('main-span')[0]
  file1 = document.getElementsByClassName('file1')[0]
  file2 = document.getElementsByClassName('file2')[0]
  file3 = document.getElementsByClassName('file3')[0]
  file4 = document.getElementsByClassName('file4')[0]
  var timeStamp = new Date().valueOf();
  // var h = document.getElementsByTagName("head")[0];
  // var s = document.getElementsByTagName("script")[0];
  const p1 = new Promise(
    function(resolve, reject) {
      var req = new XMLHttpRequest();
      req.onprogress = updateProgress1;
      req.open('GET', 'https://simplesolutions-f7-dev.s3.amazonaws.com/app.css?x=' + timeStamp, true);
      req.onreadystatechange = function (aEvt) {
      if (req.readyState == 4)
        {
          loadedFiles++
          mainSpan.innerText = 'Archivos cargados:' + loadedFiles + '/4'
          var style = document.createElement('style');
          style.type = 'text/css';
          style.textContent = req.responseText
          document.getElementsByTagName('head')[0].appendChild(style);
          resolve('ok - 1')
        }
      };
      req.send();
    }
  );
  const p2 = new Promise(
    function(resolve, reject) {
      var req = new XMLHttpRequest();
      req.onprogress = updateProgress2;
      req.open('GET', 'https://simplesolutions-f7-dev.s3.amazonaws.com/manifest.js?x=' + timeStamp, true);
      req.onreadystatechange = function (aEvt) {
      if (req.readyState == 4)
        {
          loadedFiles++
          mainSpan.innerText = 'Archivos cargados:' + loadedFiles + '/4'
          script1 = document.createElement('script');
          script1.type = 'text/javascript';
          script1.defer = true;
          script1.text = req.responseText
          // var s = document.getElementsByTagName('script')[0];
          // s.parentNode.insertBefore(script, s);
          // document.getElementsByTagName('body')[0].appendChild(script);
          resolve('ok - 2')
        }
      };
      req.send();
    }
  );
  const p3 = new Promise(
    function(resolve, reject) {
      var req = new XMLHttpRequest();
      req.onprogress = updateProgress3;
      req.open('GET', 'https://simplesolutions-f7-dev.s3.amazonaws.com/vendor.js?x=' + timeStamp, true);
      req.onreadystatechange = function (aEvt) {
        if (req.readyState == 4){
          loadedFiles++
          mainSpan.innerText = 'Archivos cargados:' + loadedFiles + '/4'
          script2 = document.createElement('script');
          script2.type = 'text/javascript';
          script2.defer = true;
          script2.text = req.responseText
          // var s = document.getElementsByTagName('script')[0];
          // s.parentNode.insertBefore(script, s);
          // document.getElementsByTagName('body')[0].appendChild(script);
          resolve('ok - 3')
        }
      };
      req.send();
    }
  );
  const p4 = new Promise(
    function(resolve, reject) {
      var req = new XMLHttpRequest();
      req.onprogress = updateProgress4;
      req.open('GET', 'https://simplesolutions-f7-dev.s3.amazonaws.com/app.js?x=' + timeStamp, true);
      req.onreadystatechange = function (aEvt) {
        if (req.readyState == 4){
          loadedFiles++
          mainSpan.innerText = 'Archivos cargados:' + loadedFiles + '/4'
          script3 = document.createElement('script');
          script3.type = 'text/javascript';
          script3.defer = true;
          script3.text = req.responseText
          // var s = document.getElementsByTagName('script')[0];
          // s.parentNode.insertBefore(script, s);
          // document.getElementsByTagName('body')[0].appendChild(script);
          resolve('ok - 4')
        }
      };
      req.send();
    }
  );
  Promise.all([p1, p2, p3, p4]).then(values => {
    console.log(values);
    splash.style.display = 'none'
    var endDate = new Date();
    var seconds = (endDate.getTime() - window.startDate.getTime()) / 1000;
    alert('Delay: ' + seconds)
    addScripts()
  });
}

function updateProgress1(evt) {
  if (evt.lengthComputable)
  {
    file1.innerText = evt.loaded.toString() + '/' + evt.total.toString()
  }
}

function updateProgress2(evt) {
  if (evt.lengthComputable)
  {
    file2.innerText = evt.loaded.toString() + '/' + evt.total.toString()
  }
}

function updateProgress3(evt) {
  if (evt.lengthComputable)
  {
    file3.innerText = evt.loaded.toString() + '/' + evt.total.toString()
  }
}

function updateProgress4(evt) {
  if (evt.lengthComputable)
  {
    file4.innerText = evt.loaded.toString() + '/' + evt.total.toString()
  }
}

function addScripts () {
  console.log('Adding scripts!')
  // var s = document.getElementsByTagName('script')[0];
  // s.parentNode.insertBefore(script, s);
  document.getElementsByTagName('body')[0].appendChild(script1);
  document.getElementsByTagName('body')[0].appendChild(script2);
  document.getElementsByTagName('body')[0].appendChild(script3);
}
