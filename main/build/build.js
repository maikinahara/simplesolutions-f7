require('./check-versions')()

process.env.NODE_ENV = 'production'

var fs = require('fs');
var ora = require('ora')
var rm = require('rimraf')
var path = require('path')
var chalk = require('chalk')
var webpack = require('webpack')
var config = require('../config')
var webpackConfig = require('./webpack.prod.conf')

var spinner = ora('building for production...')
spinner.start()

let src, dest, appToBuild
var args = process.argv
dest = './src/pages/login.vue'
if (args.length >= 3) {
  appToBuild = args[2].split('-')[1]
  console.log('> Building app: ' + appToBuild + '\n')
}

// switch (appToBuild) {
//   case 'ss':
//     src = './src/pages/login-ss.vue'
//     break;
//   case 'ayres':
//     src = './src/pages/login-ayres.vue'
//     break;
//   default:
//   src = './src/pages/login-ss.vue'
// }
//
// fs.access('./src/pages/', (err) => {
// 	if (err) console.log('ERROR: ' + err)
// 	else {
// 		let readStream = fs.createReadStream(src);
//
// 		readStream.once('error', (err) => {
// 			console.log(err);
// 		});
//
// 		readStream.once('end', () => {
// 			console.log('done copying');
//       console.log('Starting app build')
//       startBuild()
// 		});
// 		readStream.pipe(fs.createWriteStream(dest));
// 	}
// });


  rm(path.join(config.build.assetsRoot, config.build.assetsSubDirectory), err => {
    if (err) throw err
    webpack(webpackConfig, function (err, stats) {
      spinner.stop()
      if (err) throw err
      process.stdout.write(stats.toString({
        colors: true,
        modules: false,
        children: false,
        chunks: false,
        chunkModules: false
      }) + '\n\n')
      console.log(chalk.cyan('  Build complete.\n'))
      console.log(chalk.yellow(
        '  Tip: built files are meant to be served over an HTTP server.\n' +
        '  Opening index.html over file:// won\'t work.\n'
      ))
    })
  })
