
// Import Vue
import Vue from 'vue'
//Import Store
import store from './assets/store/index.js'
// import PortalVue from 'portal-vue'
// Import F7
// import Framework7 from 'framework7'

// Import F7 Vue Plugin
// import Framework7Vue from 'framework7-vue'

// Import F7 iOS Theme Styles
// import Framework7Theme from 'framework7/dist/css/framework7.ios.min.css'
// import Framework7ThemeColors from 'framework7/dist/css/framework7.ios.colors.min.css'
/* OR for Material Theme:
import Framework7Theme from 'framework7/dist/css/framework7.material.min.css'
import Framework7ThemeColors from 'framework7/dist/css/framework7.material.colors.min.css'
*/
// Import App Custom Styles
import './css/app.css'
// import '@fortawesome/fontawesome-free/css/all.min.css'
// import './assets/css/material-icons.css'
// Import Routes
import Routes from './routes.js'

// Import App Component
import App from './app'
// Import App Component
import Helper from './assets/js/helper'
import {EventBus} from './assets/js/event-bus'
import {ToolbarHelper} from './assets/js/toolbar-helper'
import {NavbarHelper} from './assets/js/navbar-helper'
import wysiwyg from './components/editor';
// import "./assets/js/editor/vueWysiwyg.css";

Vue.use(wysiwyg, {});

//Import lightbox2
//http://lokeshdhakar.com/projects/lightbox2/
// import '../node_modules/lightbox2/dist/js/lightbox-plus-jquery.min.js'
// import '../node_modules/lightbox2/dist/css/lightbox.min.css'
import helper from './assets/js/helper';

// import './css/materialicons.css'

import {mapActions, mapGetters} from 'vuex'
// Init F7 Vue Plugin
Vue.use(Framework7Vue)
Vue.prototype.$eventBus = EventBus
try {
  if(device){
    document.addEventListener("deviceready", onDeviceReady, false);
  }
} catch (e) {
  console.log('>>>error: WEB: ' + e)
  onDeviceReady()
}

function onDeviceReady() {
  try {
    StatusBar.styleLightContent()
    StatusBar.backgroundColorByHexString('#191919')
    StatusBar.overlaysWebView(false)
  } catch (err) {
    console.log(err)
  }
  console.log('Device is ready!!! loading vue')
  // Init App
  new Vue({
    el: '#app',
    template: '<app/>',
    // Init Framework7 by passing parameters here
    framework7: {
      root: '#app',
      uniqueHistory: true,
      // cache: false,
      // cacheDuration: 0,
      tapHold: true,
      routes: Routes,
      swipePanel: 'left',
      swipeout: false,
      fastClicks: true
      //no queremos pg repetidas :/
    },
    store,
    // Register App Component
    components: {
      app: App
    },
    computed: {
      ...mapGetters(['credentials', 'isToolbarLink'])
      // isToolbarLink (){
      //   return this.$store.getters.isToolbarLink
      // },
    },
    mounted () {
      this.$nextTick(()=>{
        try {
          // console.log('hiding splash!')
          navigator.splashscreen.hide()
        }
        catch (e) {
          console.log(e)
        }
        window.addEventListener('native.keyboardshow', this.keyboardShowHandler)
        window.addEventListener('native.keyboardhide', this.keyboardHideHandler)
      })
      let vm = this
      this.$$(document).on('page:afterback', ((evt, data) => {
        let str = data.page.view.activePage.url.replace(/\//g,'')
        let currentPage = str.charAt(0).toUpperCase() + str.slice(1)
        console.log('page:afterback: ' + currentPage)
        //Borro la info del modal generico y lo oculto
        switch (currentPage) {
          case 'Home':
            vm.getBadges({
              idPantalla: 1,
              idAccion: 'Notificaciones',
              idBarrio: vm.credentials.barrio.id,
              idUsuario: vm.credentials.user,
              idPassword: vm.credentials.password
            })
            vm.$store.dispatch('setShowInfo', { show: false, data: ''})
            // vm.$eventBus.$emit('onEnterHome')
            break;
          case 'Reservas':
            vm.$store.dispatch('setShowInfo', {show: false, data: ''} )
            vm.$store.dispatch('popMainTitle')
            // vm.$store.dispatch('setMainTitleToPush', 'Reservas')
            break;
          case 'Expensas':
          case 'Novedades':
          case 'Consultas':
          case 'ConsultasAdmin':
            this.setSelectAll({ currentPage: currentPage })
            break;
          default:
        }
        this.$store.dispatch('popMainTitle')
      }))
      this.$$(document).on('page:afteranimation', ((evt, data)=>{
        if (vm.$$('.cached').length > 0 ) vm.$$(vm.$$('.cached')).remove()
        // console.log('page:afteranimation')
        vm.$store.dispatch('pushMainTitle')
      }))
      this.$$(document).on('page:beforeanimation', ((evt, data) => {
        let page = data.page.url
        console.log('page:beforeanimation>>>> ' + page )
        ToolbarHelper.setToolbar(page)
        NavbarHelper.setNavbarFilterIcon(page)
        // let show = Helper.ShowSelectAllMenu(page)
        let showQueryMenu = Helper.ShowQueryMenu(page)
        // vm.setSelectAll({
        //   show: show,
        //   listRef: '',
        //   items: [],
        //   selectedItems: [],
        //   idPantalla: -1,
        //   showCheckbox: false,
        //   selectAll: false,
        //   btnText: 'Marcar como leidos',
        //   btnIcon: 'done'
        // })
        vm.toggleQueryMenu(showQueryMenu)
      }))
    },
    methods:{
      ...mapActions(['setSelectAll', 'setToolbarConfig', 'getBadges', 'toggleQueryMenu', 'setTagsData']),
      keyboardHideHandler(e){
        this.$$(".toolbar").each((i,v)=>{this.$$(v).show()})
      },
      keyboardShowHandler(e){
        this.$$(".toolbar").each((i,v)=>{this.$$(v).hide()})
      }
    }
  })
}
