const credentials = state => state.credentials
const loginsSaved = state => state.loginsSaved
const grillaMain = state => state.grillaMain
const mainGrid = state => state.mainGrid
const barHome = state => state.barHome
const alertData = state => state.alertData
const newsGrid = state => state.newsGrid
const newsConfig = state => state.newsConfig
const showInfo = state => state.showInfo
const ayresBarrios = state => state.ayresBarrios
const isAdmin = state => state.credentials.admin
const navbarTitle = state => state.navbarTitle
const pollsArray = state => state.pollsArray
const reservationData = state => state.reservationData
const reservationDetailData = state => state.reservationDetailData
const myReservationData = state => state.myReservationData
const paymentData = state => state.paymentData
const guestData = state => state.guestData
const guestGirovisionData = state => state.guestGirovisionData
const diaryData = state => state.diaryData
const infractionData = state => state.infractionData
const ballotData = state => state.ballotData
const forumData = state => state.forumData
const topicData = state => state.topicData
const saleData = state => state.saleData
const saleFilterData = state => state.saleFilterData
const mySaleData = state => state.mySaleData
const mainTitleToPush = state => state.mainTitleToPush
const mainTitle = state => state.mainTitle.length > 0 ? state.mainTitle[state.mainTitle.length -1] : ''
const mainTitleArray = state => state.mainTitle
const isToolbarLink = state => state.isToolbarLink
const mailData = state => state.mailData
const newsData = state => state.newsData
const appVersion = state => state.appVersion
const docsData = state => state.docsData
const expenseData = state => state.expenseData
const docsV3Data = state => state.docsV3Data
const toolbarOptions = state => state.toolbarOptions
const selectAll = state => state.selectAll
const toolbarHeight = state => state.toolbarHeight
const toolbarConfig = state => state.toolbarConfig
const queryData = state => state.queryData
const queryDataAdmin = state => state.queryDataAdmin
const queryMenu = state => state.queryMenu
const queryTypeData = state => state.queryTypeData
const tagsData = state => state.tagsData
const fab = state => state.fab
const queryPrivateNote = state => state.queryPrivateNote
const ingesysData = state => state.ingesysData
const isUruguay = state => state.isUruguay
const filterIconData = state => state.filterIconData

export {
  credentials,
  loginsSaved,
  grillaMain,
  mainGrid,
  barHome,
  alertData,
  newsGrid,
  newsConfig,
  ayresBarrios,
  isAdmin,
  navbarTitle,
  showInfo,
  pollsArray,
  reservationData,
  reservationDetailData,
  myReservationData,
  paymentData,
  guestData,
  guestGirovisionData,
  diaryData,
  infractionData,
  ballotData,
  forumData,
  topicData,
  saleData,
  mySaleData,
  saleFilterData,
  mainTitle,
  mainTitleArray,
  mainTitleToPush,
  isToolbarLink,
  mailData,
  newsData,
  docsData,
  appVersion,
  expenseData,
  docsV3Data,
  toolbarOptions,
  selectAll,
  toolbarHeight,
  toolbarConfig,
  queryData,
  queryDataAdmin,
  queryMenu,
  queryTypeData,
  tagsData,
  fab,
  queryPrivateNote,
  ingesysData,
  isUruguay,
  filterIconData
}
