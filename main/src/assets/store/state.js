export default {
  appVersion: '0.0.0',
  credentials: {
    barrio: {
      name: null,
      id: -1
    },
    user: null,
    password: null,
    admin: null,
    loged: false,
    token: null
  },
  loginsSaved: [],
  grillaMain: null,
  mainGrid: [],
  barHome: {
    headerImage: '',
    leftText: '',
    rightText: ''
  },
  alertData: {},
  newsGrid: {},
  zoomImageSrc: '',
  ayresBarrios: [],
  navbarTitle: {
    title: 'Simple Solutions',
    previousTitle: 'Simple Solutions',
    hasPreviousTitle: false
  },
  showInfo: {
    show: false,
    data: '<p>No hay informacion</p>'
  },
  newsConfig: {
    type: 'news',
    news: {
      title: '',
      url: 'novedades',
      newExpense: {
        title: 'Paso 1: Redactar',
        url: 'expensas',
      }
    },
    espenses: {
      title: 'Expensas',
      url: 'expensas',
      newExpense: {
        title: 'Paso 1: Redactar',
        url: 'expensas',
      }
    },
    documents: {
      title: 'Documentos',
      url: 'novedades'
    }
  },
  pollsArray: [],
  reservationData: {},
  reservationDetailData: {},
  myReservationData:{},
  paymentData: {},
  guestData: {},
  guestGirovisionData: {},
  diaryData: {},
  infractionData: {},
  ballotData: {},
  forumData: {},
  topicData: {},
  saleData: {},
  mySaleData: {},
  saleFilterData: '-1',
  mainTitle: [],
  mainTitleToPush: '',
  isToolbarLink: false,
  mailData: {},
  newsData: {},
  docsData: {},
  queryData: {},
  queryDataAdmin: {},
  expenseData: {},
  docsV3Data: {},
  toolbarConfig: {
    isAdmin: false,
    text: '',
    title: '',
    previousTitle: '',
    showToolbar: false,
    showGirovisionButton: false,
    event: ''
  }, 
  toolbarOptions: {},
  selectAll: {
    show: false,
    listRef: '',
    items: [],
    selectedItems: [],
    idPantalla: -1,
    showCheckbox: false,
    selectAll: false,
    btnText: 'Marcar como leidos',
    btnIcon: 'WP',
    currentPage: undefined
  },
  toolbarHeight: null,
  queryMenu: {
    boxes: [],
    show: false
  },
  queryTypeData: [],
  tagsData: [],
  fab: {
    show: false,
    event: ''
  },
  queryPrivateNote: {
    show: false
  },
  ingesysData: [],
  isUruguay: false,
  filterIconData: {
    show: false,
    color: '#fff',
    selector: ''
  }
}
