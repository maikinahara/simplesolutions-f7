import * as types from './mutation-types'
import Helper from '../js/helper'
export const setCredentials = ({ commit }, credentials) => {
  if (credentials) {
    commit(types.SET_CREDENTIALS, credentials)
  }
}

export const setPassword = ({ commit }, password) => {
  if (password) {
    commit(types.SET_PASSWORD, password)
  }
}

export const setLoginsSaved = ({ commit }, loginsSaved) => {
  if (loginsSaved) {
    commit(types.SET_LOGINS_SAVED, loginsSaved)
  }
}

export const setGrillaMain = ({ commit }, grillaMain) => {
  if (grillaMain) {
    commit(types.SET_GRILLA_MAIN, grillaMain)
  }
}

export const setMainGrid = ({ commit }, mainGrid) => {
  if (mainGrid) {
    commit(types.SET_MAIN_GRID, mainGrid)
  }
}

export const setBarHome = ({ commit }, barHome) => {
  if (barHome) {
    commit(types.SET_BAR_HOME, barHome)
  }
}

export const setAlertData = ({ commit }, alertData) => {
  if (alertData) {
    commit(types.SET_ALERT_DATA, alertData)
  }
}

export const setShowInfo = ({ commit }, showInfo) => {
  if (showInfo) {
    commit(types.SET_SHOW_INFO, showInfo)
  }
}

export const setNewsConfigType = ({ commit }, newsConfigType) => {
  if (newsConfigType) {
    commit(types.SET_NEWS_CONFIG_TYPE, newsConfigType)
  }
}

export const setNewsGrid = ({ commit }, newsGrid) => {
  if (newsGrid) {
    commit(types.SET_NEWS_GRID, newsGrid)
  }
}

export const setAyresBarrios = ({ commit }, ayresBarrios) => {
  if (ayresBarrios) {
    commit(types.SET_AYRES_BARRIOS, ayresBarrios)
  }
}

export const setNavbarTitle = ({ commit }, navbarTitle) => {
  if (navbarTitle) {
    commit(types.SET_NAVBAR_TITLE, navbarTitle)
  }
}

export const setPollsArray = ({ commit }, pollsArray) => {
  if (pollsArray) {
    commit(types.SET_POLLS_ARRAY, pollsArray)
  }
}

export const setReservationData = ({ commit }, reservationData) => {
  if (reservationData) {
    commit(types.SET_RESERVATION_DATA, reservationData)
  }
}

export const setMyReservationData = ({ commit }, myReservationData) => {
  if (myReservationData) {
    commit(types.SET_MY_RESERVATION_DATA, myReservationData)
  }
}

export const setPaymentData = ({ commit }, paymentData) => {
  if (paymentData) {
    commit(types.SET_PAYMENT_DATA, paymentData)
  }
}

export const setGuestData = ({ commit }, guestData) => {
  if (guestData) {
    commit(types.SET_GUEST_DATA, guestData)
  }
}

export const setGuestGirovisionData = ({ commit }, guestGirovisionData) => {
  if (guestGirovisionData) {
    commit(types.SET_GUEST_GIROVISION_DATA, guestGirovisionData)
  }
}

export const setDiaryData = ({ commit }, diaryData) => {
  if (diaryData) {
    commit(types.SET_DIARY_DATA, diaryData)
  }
}

export const setInfractionData = ({ commit }, infractionData) => {
  if (infractionData) {
    commit(types.SET_INFRACTION_DATA, infractionData)
  }
}

export const setBallotData = ({ commit }, ballotData) => {
  if (ballotData) {
    commit(types.SET_BALLOT_DATA, ballotData)
  }
}

export const setForumData = ({ commit }, forumData) => {
  if (forumData) {
    commit(types.SET_FORUM_DATA, forumData)
  }
}

export const setTopicData = ({ commit }, topicData) => {
  if (topicData) {
    commit(types.SET_TOPIC_DATA, topicData)
  }
}

export const setSaleData = ({ commit }, saleData) => {
  if (saleData) {
    commit(types.SET_SALE_DATA, saleData)
  }
}

export const setMySaleData = ({ commit }, mySaleData) => {
  if (mySaleData) {
    commit(types.SET_MY_SALE_DATA, mySaleData)
  }
}

export const setFilterData = ({ commit }, saleFilterData) => {
  if (saleFilterData) {
    commit(types.SET_FILTER_DATA, saleFilterData)
  }
}

export const setMainTitleToPush = ({ commit }, mainTitleToPush) => {
  if (mainTitleToPush || mainTitleToPush === '') {
    commit(types.SET_MAIN_TITLE_TO_PUSH, mainTitleToPush)
  }
}

export const changeMainTitle = ({ commit }, title) => {
  commit(types.CHANGE_MAIN_TITLE, title)
}

export const pushMainTitle = ({ commit }) => {
  commit(types.PUSH_MAIN_TITLE)
}

export const popMainTitle = ({ commit }) => {
  commit(types.POP_MAIN_TITLE)
}

export const clearMainTitle = ({ commit }) => {
  commit(types.CLEAR_MAIN_TITLE)
}

export const flipMainTitleArray = ({ commit }, title) => {
  commit(types.FLIP_MAIN_TITLE_ARRAY, title)
}

export const setIsToolbarLink = ({ commit }, isToolbarLink) => {
  commit(types.SET_IS_TOOLBAR_LINK, isToolbarLink)
}

export const setMailData = ({ commit }, mail) => {
  if (mail) {
    commit(types.SET_MAIL_DATA, mail)
  }
}

export const setNewsData = ({ commit }, news) => {
  if (news) {
    commit(types.SET_NEWS_DATA, news)
  }
}

export const setDocsData = ({ commit }, docs) => {
  if (docs) {
    commit(types.SET_DOCS_DATA, docs)
  }
}

export const setExpenseData = ({ commit }, expense) => {
  if (expense) {
    commit(types.SET_EXPENSE_DATA, expense)
  }
}

export const setDocsV3Data = ({ commit }, docs) => {
  if (docs) {
    commit(types.SET_DOCV3_DATA, docs)
  }
}

export const setPushwoosh = ({ commit }) => {
    commit(types.SET_PUSHWOOSH)
}

export const setAppVersion = ({ commit }, appVersion) => {
  if (appVersion) commit(types.SET_APP_VERSION, appVersion)
}

export const setToolbarOptions = ({ commit }, toolbarOptions) => {
  if (toolbarOptions) commit(types.SET_TOOLBAR_OPTIONS, toolbarOptions)
}

export const setSelectAll = ({ commit }, selectAll) => {
  if (selectAll) commit(types.SET_SELECT_ALL, selectAll)
}

export const toggleSelectedItem = ({ commit }, item) => {
  if (item) commit(types.TOGGLE_SELECTED_ITEM, item)
}

export const toggleSelectionCheckbox = ({ commit }, show) => {
  commit(types.TOGGLE_SELECTION_CHECKBOX, show)
}

export const toggleSelectAll = ({ commit }, data) => {
  if(data) commit(types.TOGGLE_SELECT_ALL, data)
}

export const toggleSelectAllShow = ({ commit }, data) => {
  if (data) commit(types.TOGGLE_SELECT_ALL_SHOW, data)
}

export const setToolbarHeight = ({ commit }, toolbarHeight) => {
  if (toolbarHeight) commit(types.SET_TOOLBAR_HEIGHT, toolbarHeight)
}

export const setToolbarConfig = ({ commit }, toolbarConfig) => {
  if (toolbarConfig) commit(types.SET_TOOLBAR_CONFIG, toolbarConfig)
}

export const setQueryData = ({ commit }, queryData) => {
  if (queryData) commit(types.SET_QUERY_DATA, queryData)
}

export const setQueryDataAdmin = ({ commit }, queryDataAdmin) => {
  if (queryDataAdmin) commit(types.SET_QUERY_DATA_ADMIN, queryDataAdmin)
}

export const setQueryBoxes = ({ commit }, boxes) => {
  if (boxes) commit(types.SET_QUERY_BOXES, boxes)
}

export const setTagsData = ({ commit }, tagsData) => {
  if (tagsData) commit(types.SET_TAGS_DATA, tagsData)
}

export const toggleQueryMenu = ({ commit }, show) => {
  commit(types.TOGGLE_QUERY_MENU, show)
}

export const getBadges = ({ commit}, data) => {
  let params = {
    url: Helper.URL.GetMain,
    noSpinner: true,
    callback: res => {
      if (res) {
        res = typeof(res) === 'string' ? JSON.parse(res) : res
        commit(types.GET_BADGES, res)
      }
    },
    data: data
  }
  Helper.post(params)
}

export const getGeneralData = ({ commit, state }) => {
  var asd = Helper
  let mainData = {
    url: Helper.URL.GetMain,
    dataType: 'json'
  }
  let userData = {
    idBarrio: state.credentials.barrio.id,
    idUsuario: state.credentials.user,
    idPassword: state.credentials.password
  }
  let paramsBoxes = {
    ...mainData,
    data: {
      idPantalla: 9,
      idAccion: 'getBandejas',
      ...userData
    }
  }
  let paramsTag = {
    ...mainData,
    data: {
      idPantalla: 9,
      idAccion: 'getEtiquetas',
      ...userData
    }
  }
  let paramsQueryTipes = {
    ...mainData,
    data: {
      idPantalla: 9,
      idAccion: 'Tipos',
      ...userData
    }
  }
  let paramsAlert = {
    ...mainData,
    data: {
      idPantalla: 22,
      idAccion: 'Inicio',
      ...userData
    }
  }
  const getBoxes = Helper.postPromise(paramsBoxes)
  const getTags = Helper.postPromise(paramsTag)
  const getQueryTipes = Helper.postPromise(paramsQueryTipes)
  const getAlerts = Helper.postPromise(paramsAlert)
  Promise.all(
    [
      getBoxes,
      getTags,
      getAlerts,
      getQueryTipes,
    ].map(p => p.catch(e => e)))
  .then(res => {
    // Validate boxes data
    if (res[0]) {
      res[0] = typeof(res[0]) === 'string' ? JSON.parse(res[0]) : res[0]
      commit(types.SET_QUERY_BOXES, res[0])
    }
    // Validate tags data
    if (res[1]) {
      res[1] = typeof(res[1]) === 'string' ? JSON.parse(res[1]) : res[1]
      commit(types.SET_TAGS_DATA, res[1])
    }
    // Validate alerts data
    if (res[2][0]) {
      res[2] = typeof(res[2]) === 'string' ? JSON.parse(res[2]) : res[2]
      if (res[2][0]) commit(types.SET_ALERT_DATA, res[2][0])
    } else commit(types.SET_ALERT_DATA, [])
    // Validate alerts data
    if (res[3]) {
      res[3] = typeof(res[3]) === 'string' ? JSON.parse(res[3]) : res[3]
      commit(types.SET_QUERY_TYPE_DATA, res[3])
    }
  })
  .catch(err => {
    console.log('err: ' + err)
  })
}

export const setFabConfig = ({ commit }, fab) => {
  if (fab) commit(types.SET_FAB_CONFIG, fab)
}

export const toggleFab = ({ commit }, show) => {
  commit(types.TOGGLE_FAB, show)
}

export const setQueryPrivateNote = ({ commit }, queryPrivateNote) => {
  if (queryPrivateNote) commit(types.SET_QUERY_PRIVATE_NOTE, queryPrivateNote)
}

export const setReservationDetailData = ({ commit }, reservationDetailData) => {
  if (reservationDetailData) commit(types.SET_RESERVATION_DETAIL_DATA, reservationDetailData)
}

export const setIngesysData = ({ commit }, ingesysData) => {
  if (ingesysData) commit(types.SET_INGESYS_DATA, ingesysData)
}

export const setIsUruguay = ({ commit }, isUruguay) => {
  commit(types.SET_IS_URUGUAY, isUruguay)
}

export const setFilterIconData = ({ commit }, filterIconData) => {
  commit(types.SET_FILTER_ICON_DATA, filterIconData)
}
