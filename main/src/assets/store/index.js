import Vue from 'vue'
import Vuex from 'vuex'
import state from './state'
import * as actions from './actions'
import * as getters from './getters'
import * as types from './mutation-types'
import _ from 'lodash'
import PW from '../js/pushwoosh'
Vue.use(Vuex)

const store = new Vuex.Store({
  strict: true,
  state,
  actions,
  getters,
  mutations : {
    [types.SET_CREDENTIALS] (state, credentials) {
      state.credentials = credentials
    },
    [types.SET_PASSWORD] (state, password) {
      state.credentials.password = password
    },
    [types.SET_LOGINS_SAVED] (state, loginsSaved) {
      state.loginsSaved = loginsSaved
    },
    [types.SET_GRILLA_MAIN] (state, grillaMain) {
      state.grillaMain = grillaMain
    },
    [types.SET_MAIN_GRID] (state, mainGrid) {
      state.mainGrid = mainGrid
    },
    [types.SET_BAR_HOME] (state, barHome) {
      state.barHome = barHome
    },
    [types.SET_ALERT_DATA] (state, alertData) {
      // if(state.credentials.barrio.id === '1156') alert(JSON.stringify(alertData, null, 2))
      state.alertData = alertData
    },
    [types.SET_NEWS_GRID] (state, newsGrid) {
      state.newsGrid = newsGrid
    },
    [types.SET_NEWS_CONFIG_TYPE] (state, newsConfigType) {
      state.newsConfig.type = newsConfigType
    },
    [types.SET_SHOW_INFO] (state, showInfo) {
      state.showInfo = showInfo
    },
    [types.SET_AYRES_BARRIOS] (state, ayresBarrios) {
      state.ayresBarrios = ayresBarrios
    },
    [types.SET_NAVBAR_TITLE] (state, navbarTitle) {
      state.navbarTitle = navbarTitle
    },
    [types.SET_POLLS_ARRAY] (state, pollsArray) {
      state.pollsArray = pollsArray
    },
    [types.SET_RESERVATION_DATA] (state, reservationData) {
      state.reservationData = reservationData
    },
    [types.SET_RESERVATION_DETAIL_DATA] (state, reservationDetailData) {
      state.reservationDetailData = reservationDetailData
    },
    [types.SET_MY_RESERVATION_DATA] (state, myReservationData) {
      state.myReservationData = myReservationData
    },
    [types.SET_PAYMENT_DATA] (state, paymentData) {
      state.paymentData = paymentData
    },
    [types.SET_GUEST_DATA] (state, guestData) {
      state.guestData = guestData
    },
    [types.SET_GUEST_GIROVISION_DATA] (state, guestGirovisionData) {
      state.guestGirovisionData = guestGirovisionData
    },
    [types.SET_DIARY_DATA] (state, diaryData) {
      state.diaryData = diaryData
    },
    [types.SET_INFRACTION_DATA] (state, infractionData) {
      state.infractionData = infractionData
    },
    [types.SET_BALLOT_DATA] (state, ballotData) {
      state.ballotData = ballotData
    },
    [types.SET_FORUM_DATA] (state, forumData) {
      state.forumData = forumData
    },
    [types.SET_TOPIC_DATA] (state, topicData) {
      state.topicData = topicData
    },
    [types.SET_SALE_DATA] (state, saleData) {
      state.saleData = saleData
    },
    [types.SET_MY_SALE_DATA] (state, mySaleData) {
      state.mySaleData = mySaleData
    },
    [types.SET_FILTER_DATA] (state, saleFilterData) {
      state.saleFilterData = saleFilterData
    },
    [types.CHANGE_MAIN_TITLE] (state, title) {
      let pos = state.mainTitle.length -1
      // Force reactivity
      let _tmp = state.mainTitle
      state.mainTitle = null
      state.mainTitle = _tmp
      state.mainTitle[pos] = title
    },
    [types.PUSH_MAIN_TITLE] (state) {
      if(state.mainTitleToPush !== '') state.mainTitle.push(state.mainTitleToPush)
    },
    [types.SET_MAIN_TITLE_TO_PUSH] (state, mainTitleToPush) {
      state.mainTitleToPush = mainTitleToPush
    },
    [types.POP_MAIN_TITLE] (state) {
      state.mainTitle.pop()
    },
    [types.FLIP_MAIN_TITLE_ARRAY] (state, title) {
      state.mainTitleToPush = ''
       _.pullAt(state.mainTitle, _.indexOf(state.mainTitle, title))
       state.mainTitle.push(title)
    },
    [types.CLEAR_MAIN_TITLE] (state) {
      state.mainTitle = []
    },
    [types.SET_IS_TOOLBAR_LINK] (stat, isToolbarLink) {
      state.isToolbarLink = isToolbarLink
    },
    [types.SET_MAIL_DATA] (state, mail) {
      state.mailData = mail
    },
    [types.SET_NEWS_DATA] (state, news) {
      state.newsData = news
    },
    [types.SET_DOCS_DATA] (state, docs) {
      state.docsData = docs
    },
    [types.SET_EXPENSE_DATA] (state, expense) {
      state.expenseData = expense
    },
    [types.SET_DOCV3_DATA] (state, docs) {
      state.docsV3Data = docs
    },
    [types.SET_PUSHWOOSH] (state) {
      try {
          if (device.platform === "Android") {
            PW.android.init()
            console.log('PWAndroid registered!')
          }
          if (device.platform === "iPhone" || device.platform === "iOS") {
            PW.iOS.init()
            console.log('PWIos registered!')
          }
          if (device.platform === "Win32NT") {
            PW.windowsPhone.init()
          }
      } catch (err) {
        console.warn(err);
      }
    },
    [types.SET_APP_VERSION] (state, appVersion) {
      state.appVersion = appVersion
    },
    [types.SET_TOOLBAR_OPTIONS] (state, toolbarOptions) {
      state.toolbarOptions = toolbarOptions
    },
    [types.SET_SELECT_ALL] (state, selectAll) {
      state.selectAll = selectAll
    },
    [types.TOGGLE_SELECTED_ITEM] (state, item) {
      let exist = _.find(state.selectAll.selectedItems, i => {
        return i === item
      })
      if(exist) _.remove(state.selectAll.selectedItems, i => { return i === item })
      else state.selectAll.selectedItems.push(item)
    },
    [types.TOGGLE_SELECTION_CHECKBOX] (state, show) {
      state.selectAll.showCheckbox = show
    },
    [types.TOGGLE_SELECT_ALL] (state, data) {
      if (data.selectAll) {
        let arr = []
        for (let i = 0; i < state.selectAll.items.length; i ++) {
          // let item = state.selectAll.items[i].idNtc
          let item = state.selectAll.items[i][data.key]
          let exist = _.find(state.selectAll.selectedItems, i => {
            return i === item
          })
          if(!exist) arr.push(item)
        }
        let newArr = _.concat(state.selectAll.selectedItems, arr)
        state.selectAll.selectedItems = newArr
        state.selectAll.listRef.forEach( l => {
          l.getElementsByTagName('input')[0].checked = true
        })
      } else {
        state.selectAll.listRef.forEach( l => {
          state.selectAll.selectedItems = []
          l.getElementsByTagName('input')[0].checked = false
        })
      }
    },
    [types.TOGGLE_SELECT_ALL_SHOW] (state, data) {
      state.selectAll.show = data.show
      state.selectAll.btnIcon = data.button
    },
    [types.SET_TOOLBAR_HEIGHT] (state, toolbarHeight) {
      state.toolbarHeight = toolbarHeight
    },
    [types.SET_TOOLBAR_CONFIG] (state, toolbarConfig) {
      state.toolbarConfig = toolbarConfig
    },
    [types.SET_QUERY_DATA] (state, queryData) {
      state.queryData = queryData
    },
    [types.SET_QUERY_DATA_ADMIN] (state, queryDataAdmin) {
      state.queryDataAdmin = queryDataAdmin
    },
    [types.GET_BADGES] (state, res) {
      res.forEach(e => {
        let grillaItem = state.mainGrid.find(x => x.idSecDesc == e.idSec)
        if (grillaItem) {
          grillaItem.badge = parseInt(e.notificaciones)
        }
      })
      // Force update
      state.mainGrid = _.cloneDeep(state.mainGrid)
    },
    [types.TOGGLE_QUERY_MENU] (state, show) {
      state.queryMenu.show = show
    },
    [types.SET_QUERY_BOXES] (state, boxes) {
      state.queryMenu.boxes = boxes
    },
    [types.SET_QUERY_TYPE_DATA] (state, queryTypeData) {
      state.queryTypeData = queryTypeData
    },
    [types.SET_TAGS_DATA] (state, tagsData) {
      state.tagsData = tagsData
    },
    [types.SET_FAB_CONFIG] (state, fab) {
      state.fab = fab
    },
    [types.TOGGLE_FAB] (state, show) {
      state.fab.show = show
    },
    [types.SET_QUERY_PRIVATE_NOTE] (state, queryPrivateNote) {
      state.queryPrivateNote = queryPrivateNote
    },
    [types.SET_INGESYS_DATA] (state, ingesysData) {
      state.ingesysData = ingesysData
    },
    [types.SET_IS_URUGUAY] (state, isUruguay) {
      state.isUruguay = isUruguay
    },
    [types.SET_FILTER_ICON_DATA] (state, filterIconData) {
      state.filterIconData = filterIconData
    }
  }
})

export default store
