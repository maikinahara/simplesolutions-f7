// Import Vue
import Vue from 'vue'
//Import F7
// import Framework7 from 'framework7'
// Import F7 Vue Plugin
// import Framework7Vue from 'framework7-vue'
let Moment = moment
//Import store
import store from '../store'
import he from 'he'
import menuIcons from './helper-icons';

let helper = {}
helper.vue = new Vue({store, framework7: {}})
helper.toolbarOptions = 'NOVEDADES|EXPENSAS|CONSULTAS|CONSULTASADMIN'
helper.mainGrid = "NOVEDADES|EXPENSAS|CONSULTAS|CONSULTASADMIN|DESCARGAS|RESERVAS|TELEFONOS|INFORMEPAGO|INVITADOS|FOROS|ENCUESTAS|BOLETAS|INVITADOSGIROVISION|CLASIFICADOS|AGENDA|COMISIONES|INFRACCIONES|CAMARAS|CORRESPONDENCIA|INVITADOSINGESYS|NOVEDADESCASTORES|GUIABARRIAL"
helper.menuIcons = menuIcons;

helper.findObjectByKey = ((array, key, value) => {
  for (let i = 0; i < array.length; i++) {
    if (array[i][key] === value) {
      return array[i]
    }
  }
  return null
})

helper.compare = ((a, b) => {
  if (a.order < b.order)
    return -1
  if (a.order > b.order)
    return 1
  return 0
})


helper.convertDate = ((format, data) => {
  switch (format) {
    case 'dd/mm/yyyy':
      const tmp = (data).split('-')
      return (tmp[2] + '/' + tmp[1] + '/'+tmp[0])
      break;
    default:
  }
})

helper.isAdminUser = ((data) => {
  try {
    let _adm = data.welcomeMessage.split('|');
    if (_adm[1] == '1') {
      window.localStorage.setItem("usuarioAdministrador", true)
      return true
    } else {
      window.localStorage.setItem("usuarioAdministrador", false)
      return false
    }
  } catch (e) {
    console.log('isAdmin Error: ' + e)
  }
})
helper.URL = {
  GetRubros: 'https://www.simplesolutions.com.ar/site/app/barrios.php',
  GetMain: 'https://www.simplesolutions.com.ar/site/app/main.php',
  GetMainTest: 'https://www.simplesolutions.com.ar/site/app/testapp.php',
  GetLoginAyres: 'https://simplesolutions.com.ar/site/app/ayres.php',
  GetMainMod: 'https://simplesolutions.com.ar/site/app/main.php',
  EnviarConsultas: 'https://www.simplesolutions.com.ar/site/app_consultas.php',
  InformePago: 'https://www.simplesolutions.com.ar/site/app_informe_pago.php',
  FileUpload: 'https://www.simplesolutions.com.ar/site/app_upload.php',
  Invitado: 'https://www.simplesolutions.com.ar/site/app_invitados.php'
}

let spinnersCount = 0;

helper.encode = (s) => {
  let enc = ''
  let str = ''
  let k = '123'
  str = s.toString();
  for (var i = 0; i < s.length; i++) {
    let a = s.charCodeAt(i)
    let b = a ^ k
    enc = enc + String.fromCharCode(b);
  }
  return enc
}

helper.checkConnection = () =>{
  return new Promise((resolve, reject) => {
    let self = helper.vue
    self.$$.ajax({
      method: 'GET',
      url: 'https://simplesolutions.com.ar/site/app/blackie.png',
      success: () => {
        resolve()
      },
      error: () => {
        console.log('no connection')
        reject()
      }
    })
  })
}

helper.post = ((params)=>{
  let self = helper.vue
  let f7 = Framework7
  // let _data = helper.encode(JSON.stringify(params.data))
  // params.data = {}
  // params.data.data = _data
  helper.checkConnection()
    .then( () => {
      self.$$.ajax({
        method: 'POST',
        url: params.url,
        data: params.data || {},
        dataType: params.dataType,
        beforeSend: function() {
          if(!params.noSpinner) {
            spinnersCount++;
            if (spinnersCount === 1)
              self.$f7.popup('.popup-preloader', true, false)
          }
        },
        complete: function() {
          if(!params.noSpinner) {
            spinnersCount--;
            if (spinnersCount === 0) 
              self.$f7.closeModal('.popup-preloader', false)
          }
        },
        success: function (data, status, xhr) {
          return params.callback(data)
        },
        error: function(xhr, status){
          try {
            self.$$.ajax({
              method: 'POST',
              url: 'https://www.simplesolutions.com.ar/site/app/main.php',
              dataType: 'json',
              data: {
                idPantalla: 999,
                idAccion: 'errorLog',
                idBarrio: store.getters.credentials.barrio.id,
                idUsuario: store.getters.credentials.user,
                idPassword: store.getters.credentials.password,
                accion: 'POST ERROR AJAX',
                error: 'status: ' + status + ' | xhr: ' + JSON.stringify(xhr, null, 4)
              },
              success: function (data, status, xhr) {
                console.log('error log ok')
              },
              error: function(xhr, status){
                console.log('error log error: ' + JSON.stringify(xhr, null, 4))
              }
            })
          } catch (err) {
            console.log('[ERROR]>>>Sending log to server: ' + err)
          }
          // console.warn(JSON.stringify(xhr))
          // console.log(JSON.stringify(status))
        }
      })
    })
    .catch(err => {
      helper.showMessage('Lo sentimos, debe tenero conexion a internet para usar la aplicación',
      function() {
        // navigator.app.exitApp()
      })
    })
})

helper.postPromise = params => {
  return new Promise((resolve, reject) => {
    let self = helper.vue
    // let _data = helper.encode(JSON.stringify(params.data))
    // params.data = {}
    // params.data.data = _data
    self.$$.ajax({
      method: 'POST',
      url: params.url,
      data: params.data || {},
      dataType: params.dataType,
      beforeSend: function() {
        if(!params.noSpinner) {
          spinnersCount++;
          if (spinnersCount === 1)
            self.$f7.popup('.popup-preloader', true, false)
        }
      },
      complete: function() {
        if(!params.noSpinner) {
          spinnersCount--;
          if (spinnersCount === 0) 
            self.$f7.closeModal('.popup-preloader', false)
        }
      },
      success: function (data, status, xhr) {
        return resolve(data)
      },
      error: function(xhr, status){
        try {
          self.$$.ajax({
            method: 'POST',
            url: 'https://www.simplesolutions.com.ar/site/app/main.php',
            dataType: 'json',
            data: {
              idPantalla: 999,
              idAccion: 'errorLog',
              idBarrio: store.getters.credentials.barrio.id,
              idUsuario: store.getters.credentials.user,
              idPassword: store.getters.credentials.password,
              accion: 'POST ERROR AJAX',
              error: 'status: ' + status + ' | xhr: ' + JSON.stringify(xhr, null, 4)
            },
            success: function (data, status, xhr) {
              console.log('error log ok')
            },
            error: function(xhr, status){
              console.log('error log error: ' + JSON.stringify(xhr, null, 4))
            }
          })
        } catch (err) {
          console.log('[ERROR]>>>Sending log to server: ' + err)
        }
        reject(xhr)
        // console.warn(JSON.stringify(xhr))
        // console.log(JSON.stringify(status))
      }
    })
  })
}

helper.downloadFile = ( url =>{
  try {
    if (device.platform === "Android") {
      window.open(url, '_system', 'EnableViewPortScale=yes');
    } else {
      window.open(url, '_blank', 'EnableViewPortScale=yes');
    }
  } catch (err) {
    helper.showMessage(err)
    let a = document.createElement('a')
    let fileName = url.split('/')[url.split('/').length-1]
    a.setAttribute('target', '_blank')
    a.setAttribute('download', fileName || '');
    a.href = url
    a.click()
  }
})

helper.setTitle = ((title)=>{
  let self = helper.vue
  self.$store.dispatch('setNavbarTitle', title)
})

helper.attachFile = ((callback)=>{
    try {
      const pictureSource = navigator.camera.PictureSourceType
      const destinationType = navigator.camera.DestinationType
        navigator.notification.confirm("Adjuntar imagen desde:",
        function (i) {
            if (i == 1) {
              // navigator.camera.getPicture(callback, function(err){alert(err)}, {
               navigator.camera.getPicture(callback, function(err){helper.showMessage('Error adjuntando imagen de galeria ' + err)}, {
                    quality: 50,
                    destinationType: destinationType.FILE_URI
                });
            }
            else {
                // navigator.camera.getPicture(callback, function(err){alert(err)}, {
                navigator.camera.getPicture(callback, function(err){helper.showMessage('Error adjuntando imagen de camara ' + err)}, {
                    quality: 50,
                    destinationType: destinationType.FILE_URI,
                    sourceType: pictureSource.PHOTOLIBRARY
                });
            }
        }, "Adjuntar", "Cámara, Galería")
    }
    catch (err) {
      helper.showMessage('Error adjuntando imagen + err: ' + err)
      callback('https://1.bp.blogspot.com/-5bPNsF5plzw/VnJWs-7RbrI/AAAAAAAARmA/DaZmn8YUjAk/s1600-r/logo_research_at_google_color_1x_web_512dp.png')
      console.log(err);
    }
})

helper.showMessage = ((message, callback, appTitle, button)=>{
  // intento alerta nativa, sino default js
  let title = ''
  try {
    title = (appTitle || store.getters.credentials.barrio.name)
  } catch (error) {
    title = 'Simple Solutions'
  }
  let buttonText = ( button || ['Aceptar'])
  try {
    navigator.notification.alert(message, callback, title, buttonText);
  } catch (e) {
    console.log('>>>error navigator.notification.alert: ' + e)
    alert(message)
    if(callback) callback()
  }
})

helper.confirm = ((message, callback = null, appTitle, buttons) => {
  let title
  try {
    title = (appTitle || helper.vue.$store.getters.credentials.barrio.name)
  } catch (e) {
    console.log(e)
    title = 'Simple Solutions'
  }
  try {
    navigator.notification.confirm(message, function(res){ return callback(res)}, title, (buttons || ['SI','NO']));
  } catch (e) {
    console.log('>>>error navigator.notification.confirm: ' + e)
    let res = confirm(message)
    return callback(res)
  }
})

helper.saveFile = ((image, idBarrio, callbackSuccess, callbackFail)=>{
  // console.log('helper.saveFile')
  // console.log('image nombre:')
  // console.log(image)
  // alert('image nombre:')
  // alert(image)
  let uploadName = helper.Dates.dbFormat() + "_" + Math.floor((Math.random() * 1000) + 1)
  // console.log('uploadName')
  const imageName = image.split("/").pop()
  // console.log('imageName')
  let extension = helper.getFileExtension(imageName)
  if (extension == "") {
    extension = ".jpg"
  }
  uploadName += extension
  // console.log('uploadName += extension')
  // console.log(uploadName += extension)
  let options = new FileUploadOptions()
  options.fileKey = "adjunto";
  options.fileName = uploadName;
  options.mimeType = "image/jpeg";
  options.chunkedMode = false;
  let params = {
    idBarrio: idBarrio
  }
  options.params = params;
  let ft = new FileTransfer();
  ft.upload(image, helper.URL.FileUpload,
    function(data) {
    let res = {
      data: data,
      uploadName: uploadName
    }
    callbackSuccess(res)
  },
  function(err){
    callbackFail(err)
  }, options, true)
})

helper.Dates = {
  dbFormat: ((date)=>{
    try {
      if(date){
        return new Date(date).toISOString().slice(0,10).replace(/-/g,"");
      }
      else{
        return new Date().toISOString().slice(0,10).replace(/-/g,"");
      }
    } catch (e) {
      console.log(e)
    }

  }),
  commonFormat: date => {
    let moment = Moment
    try {
      if(date){
        // let _date = new Date(date).toISOString().slice(0,10).split("-").reverse().join("/")
        // console.log("_date: " + _date)
        return moment(date).format('DD/MM/YYYY')
      }
      else{
        return moment().format('DD/MM/YYYY')
        // return new Date().toISOString().slice(0,10).split("-").reverse().join("/")
      }
    } catch (e) {
      console.log('error parse date: ' + e)
    }
  },
  getHours: ((date)=>{
    try {
      if(date){
        return date.slice(11,19).split(':')[0]
      }
    } catch (e) {
      console.log(e)
    }
  }),
  getMinutes: ((date)=>{
    try {
      if(date){
        return date.slice(11,19).split(':')[1]
      }
    } catch (e) {
      console.log(e)
    }
  }),
  getSeconds: ((date)=>{
    try {
      if(date){
        return date.slice(11,19).split(':')[2]
      }
    } catch (e) {
      console.log(e)
    }
  })
}

helper.getFileExtension = ((filename)=>{
  let fileExt = ''
  let extension = ''
  try {
    fileExt = filename.substr((filename.lastIndexOf('.') + 1));
  } catch (e) {
    console.log('>>>error getFileExtension: ' + e)
  } finally {
    switch (fileExt) {
      case 'jpg':
        extension = ".jpg"
        break
      case 'jpeg':
        extension = ".jpeg"
        break
      case 'png':
        extension = ".png"
        break
      case 'gif':
        extension = ".gif"
        break
      case 'zip':
        extension = ".zip"
        break
      case 'rar':
        extension = ".rar"
        break
      case 'pdf':
        extension = ".pdf"
        break
      case 'xls':
        extension = ".xls"
        break
      case 'xlsx':
        extension = ".xlsx"
        break
      case 'csv':
        extension = ".csv"
        break
      case 'doc':
        extension = ".doc"
        break
      case 'docx':
        extension = ".docx"
        break
      case 'txt':
        extension = ".txt"
        break
      case 'ppt':
        extension = ".ppt"
        break
      case 'pptx':
        extension = ".pptx"
        break
      default:
        extension = ""
        break
    }
    return extension
  }
})

helper.encodeHTML = ((html)=>{
  let escape = document.createElement('textarea')
  escape.textContent = html
  return escape.innerHTML
})

helper.decodeHTML = ((text)=>{
  return (he.decode(he.decode(text)))
})

helper.ShowSelectAllMenu = page => {
  switch (page) {
    case 'expensas':
      return true
      break
    case 'novedades':
      return true
      break
    case 'consultasadmin':
      return true
      break
    default:
      return false
      break
  }
}

helper.ShowQueryMenu = page => {
  switch (page) {
    case 'consultasadmin':
      return true
      break
    default:
      return false
      break
  }
}

helper.getAppVersion = () => {
  return new Promise((resolve, reject) => {
    try {
      let version = ''
      let packageName = ''
      cordova.getAppVersion.getVersionNumber()
        .then(_version => {
          version = _version
          return cordova.getAppVersion.getPackageName()
        })
        .then((_packageName)=> {
          packageName = _packageName
          let rta = version + '|' + packageName
          resolve(rta)
        })
    } catch (e) {
      reject(e)
    }
  })
}


helper.getSelectAllKey = page => {
  switch(page) {
    case 'novedades':
      return 'idNtc'
    case 'expensas':
      return 'idNtc'
    case 'consultasadmin':
      return 'id_consulta'
      case 'consultas':
      return 'id_consulta'
    default:
      return ''
  }
}

helper.isUruguay = () => {
  switch (window.APPTORUN) {
    case 'carmel':
      store.commit('SET_IS_URUGUAY', true)
      break;
    default:
      store.commit('SET_IS_URUGUAY', false)
      break;
  }
}

helper.calendarData = {
    monthNames: {
      esp: 'Enero|Febrero|Marzo|Abril|Mayo|Junio|Julio|Agosto|Septiembre|Octubre|Noviembre|Diciembre'.split('|')
    },
    monthNamesShort: {
      esp: 'Ene|Feb|Mar|Abr|May|Jun|Jul|Ago|Sep|Oct|Nov|Dic'.split('|')
    },
    dayNames: {
      esp: 'Domingo|Lunes|Martes|Miercoles|Jueves|Viernes|Sabado'.split('|')
    },
    dayNamesShort: {
      esp: 'Dom|Lun|Mar|Mie|Jue|Vie|Sab'.split('|')
    }
  }
export default helper
