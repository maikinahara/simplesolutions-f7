const hammer = {};

function setZoomEvents(el, context) {
  for (let i = 0; i < el.length; i++) {
    let timeStamp = new Date().valueOf();
    let org_html = el[i];
    let div = document.createElement("div");
    div.id = timeStamp;
    div.innerHTML = el[i].outerHTML;
    div.style.zIndex = '9999';
    div.style.overflow = 'auto';
    org_html.parentNode.appendChild(div);
    org_html.parentNode.removeChild(org_html);
    // document.getElementById(timeStamp).innerHTML = new_html
    let parent = document.getElementById(timeStamp);
    newHammer(el[i], parent, context);
  }
}

function newHammer(elImg, parent, context) {
  let image = elImg;
  let mc = new Hammer.Manager(image);
  let pinch = new Hammer.Pinch();
  let pan = new Hammer.Pan();
  // pinch.recognizeWith(pan);
  mc.add([pinch, pan]);
  // mc.add([pinch]);
  let adjustScale = 1;
  let adjustDeltaX = 0;
  let adjustDeltaY = 0;
  let currentScale = null;
  let currentDeltaX = null;
  let currentDeltaY = null;
  let maxScale = 3;
  let minScale = 1;

  // Prevent long press saving on mobiles.

  parent.addEventListener("touchstart", function(e) {
    e.preventDefault();
  });

  // Handles pinch and pan events/transforming at the same time;
  mc.on("pinch", function(ev) {
    context.$f7.params.swipePanel = false
    console.log('pinch + swipepanel: ' + context.$f7.params.swipePanel)
    var transforms = [];
    // Adjusting the current pinch/pan event properties using the previous ones set when they finished touching
    let newScale = adjustScale * ev.scale;
    currentScale = Math.max(Math.min(newScale, maxScale), minScale);
    transforms.push("scale(" + currentScale + ")");
    image.style.transform = transforms.join(" ");
    //parent.style.transform = transforms.join(" ");
  });

  mc.on('panstart', ev => {
    console.log('panstart: ' + ev.type)
    f7.params.swipePanel = false
  })
  // Handles pinch and pan events/transforming at the same time;
  mc.on("panleft panright panup pandown", function(ev) {
    // context.$f7.params.swipePanel = false
    context.$nextTick(() => {
      console.log('panmove + swipepanel: ' + context.$f7.params.swipePanel)
      let transforms = [];
      currentDeltaX = adjustDeltaX + ev.deltaX / currentScale;
      currentDeltaY = adjustDeltaY + ev.deltaY / currentScale;
      transforms.push("scale(" + currentScale + ")");
      transforms.push(
        "translate(" + currentDeltaX + "px," + currentDeltaY + "px)"
      );
      image.style.transform = transforms.join(" ");
      // parent.style.transform = transforms.join(" ");
    })
  });

  mc.on("panend", ev => {
    context.$f7.params.swipePanel = 'left'
  });
  
  mc.on("pinchend", function(ev) {
    context.$f7.params.swipePanel = 'left'
    // Saving the final transforms for adjustment next time the user interacts.
    adjustScale = currentScale;
    adjustDeltaX = currentDeltaX;
    adjustDeltaY = currentDeltaY;
  });
};

function addZoom (elm) {
  let hammertime = new Hammer(elm, {});
  hammertime.get('pinch').set({
    enable: true
  });
  let posX = 0,
    posY = 0,
    scale = 1,
    last_scale = 1,
    last_posX = 0,
    last_posY = 0,
    max_pos_x = 0,
    max_pos_y = 0,
    transform = "",
    el = elm;

  hammertime.on('doubletap pan pinch panend pinchend', function(ev) {
    if (ev.type == "doubletap") {
      transform =
        "translate3d(0, 0, 0) " +
        "scale3d(2, 2, 1) ";
      scale = 2;
      last_scale = 2;
      try {
        if (window.getComputedStyle(el, null).getPropertyValue('-webkit-transform').toString() != "matrix(1, 0, 0, 1, 0, 0)") {
          transform =
            "translate3d(0, 0, 0) " +
            "scale3d(1, 1, 1) ";
          scale = 1;
          last_scale = 1;
        }
      } catch (err) {}
      el.style.webkitTransform = transform;
      transform = "";
    }

    //pan
    if (scale != 1) {
      posX = last_posX + ev.deltaX;
      posY = last_posY + ev.deltaY;
      max_pos_x = Math.ceil((scale - 1) * el.clientWidth / 2);
      max_pos_y = Math.ceil((scale - 1) * el.clientHeight / 2);
      if (posX > max_pos_x) {
        posX = max_pos_x;
      }
      if (posX < -max_pos_x) {
        posX = -max_pos_x;
      }
      if (posY > max_pos_y) {
        posY = max_pos_y;
      }
      if (posY < -max_pos_y) {
        posY = -max_pos_y;
      }
    }

    //pinch
    if (ev.type == "pinch") {
      scale = Math.max(.999, Math.min(last_scale * (ev.scale), 4));
    }
    if (ev.type == "pinchend") {
      last_scale = scale;
    }

    //panend
    if (ev.type == "panend") {
      last_posX = posX < max_pos_x ? posX : max_pos_x;
      last_posY = posY < max_pos_y ? posY : max_pos_y;
    }

    if (scale != 1) {
      transform =
        "translate3d(" + posX + "px," + posY + "px, 0) " +
        "scale3d(" + scale + ", " + scale + ", 1)";
    }

    if (transform) {
      el.style.webkitTransform = transform;
    }
  });
}
export default { setZoomEvents, newHammer, addZoom };
