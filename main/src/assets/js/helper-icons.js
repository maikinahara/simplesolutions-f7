// Base Icons
import ssExpensasIcon from '../img/icons/home/expensas.png';
import ssBoletasIcon from '../img/icons/home/expensas.png';
import ssNovedadesIcon from '../img/icons/home/novedades.png';
import ssReservasIcon from '../img/icons/home/eventos.png';
import ssDescargasIcon from '../img/icons/home/documentos.png';
import ssConsultasIcon from '../img/icons/home/consultas.png';
import ssTelefonosIcon from '../img/icons/home/telefonos.png';
import ssInformarPagoIcon from '../img/icons/home/informar-pago.png';
import ssInvitadosIcon from '../img/icons/home/invitados.png';
import ssForoIcon from '../img/icons/home/foro.png';
import ssEncuestasIcon from '../img/icons/home/encuestas.png';
// import ssInvitadosGiroIcon from '../img/icons/home/invitadosGiro.png'
import ssInvitadosGiroIcon from '../img/icons/home/invitados.png';
import ssClasificadosIcon from '../img/icons/home/clasificados.png';
import ssCamaraIcon from '../img/icons/home/camara.png';
import ssInfraccionIcon from '../img/icons/home/stop.png';
import ssAgendaIcon from '../img/icons/home/clock2.png';
import ssComisionesIcon from '../img/icons/home/comisiones.png';
import ssCorrespondenciaIcon from '../img/icons/home/correspondencia.png';
const ssGuiaBarrialIcon = 'https://simplesolutions.com.ar/site/app/guia%20barrial%20APP.png';

// Pampa icons
import pampaReservasIcon from '../img/icons/home/pampa/calendar.svg';
import pampaInformarPagoIcon from '../img/icons/home/pampa/checked.svg';
import pampaEncuestasIcon from '../img/icons/home/pampa/discuss-issue.svg';
import pampaNovedadesIcon from '../img/icons/home/pampa/megaphone.svg';
import pampaClasificadosIcon from '../img/icons/home/pampa/cart.svg';
import pampaForoIcon from '../img/icons/home/pampa/network.svg';
import pampaTelefonosIcon from '../img/icons/home/pampa/smartphone.svg';
import pampaConsultasIcon from '../img/icons/home/pampa/chat.svg';
import pampaAgendaIcon from '../img/icons/home/pampa/diary.svg';
import pampaDescargasIcon from '../img/icons/home/pampa/inbox.svg';
import pampaBoletasIcon from '../img/icons/home/pampa/invoice.svg';
import pampaCorrespondenciaIcon from '../img/icons/home/pampa/message.svg';
import pampaExpensasIcon from '../img/icons/home/pampa/office.svg';
import pampaInfraccionIcon from '../img/icons/home/pampa/stop.svg';
// import pampaNewspaperIcon from '../img/icons/home/pampa/newspaper.svg';

const menuIcons = {
  expensas: `<img src=${ssExpensasIcon} height="40" />`,
  boletas: `<img src=${ssBoletasIcon} height="40" />`,
  novedades: `<img src=${ssNovedadesIcon} height="40" />`,
  novedadescastores: `<img src=${ssNovedadesIcon} height="40" />`,
  reservas: `<img src=${ssReservasIcon} height="40" />`,
  descargas:   `<img src=${ssDescargasIcon} height="40" />`,
  consultas: `<img src=${ssConsultasIcon} height="40" />`,
  consultasadmin: `<img src=${ssConsultasIcon} height="40" />`,
  telefonos: `<img src=${ssTelefonosIcon} height="40" />`,
  informepago: `<img src=${ssInformarPagoIcon} height="40" />`,
  invitados: `<img src=${ssInvitadosIcon} height="40" />`,
  foros: `<img src=${ssForoIcon} height="40" />`,
  encuestas: `<img src=${ssEncuestasIcon} height="40" />`,
  invitadosgirovision: `<img src=${ssInvitadosGiroIcon} height="40" />`,
  clasificados: `<img src=${ssClasificadosIcon} height="40" />`,
  camaras: `<img src=${ssCamaraIcon} height="40" />`,
  infracciones: `<img src=${ssInfraccionIcon} height="40" />`,
  agenda: `<img src=${ssAgendaIcon} height="40" />`,
  comisiones: `<img src=${ssComisionesIcon} height="40" />`,
  correspondencia: `<img src=${ssCorrespondenciaIcon} height="40" />`,
  invitadosingesys: `<img src=${ssInvitadosIcon} height="40" />`,
  guiabarrial: `<img src=${ssGuiaBarrialIcon} height="40" />`
};

if (window.APPTORUN === 'pampa') {
  menuIcons['reservas'] = `<img src=${pampaReservasIcon}  />`;
  menuIcons['boletas'] = `<img src=${pampaBoletasIcon}  />`;
  menuIcons['informepago'] = `<img src=${pampaInformarPagoIcon}  />`;
  menuIcons['encuestas'] = `<img src=${pampaEncuestasIcon}  />`;
  menuIcons['novedades'] = `<img src=${pampaNovedadesIcon}  />`;
  menuIcons['clasificados'] = `<img src='https://simplesolutions.com.ar/site/app/newspaper.svg'/>`;
  menuIcons['foros'] = `<img src=${pampaForoIcon}  />`;
  menuIcons['telefonos'] = `<img src=${pampaTelefonosIcon}  />`;
  menuIcons['consultas'] = `<img src=${pampaConsultasIcon}  />`;
  menuIcons['consultasadmin'] = `<img src=${pampaConsultasIcon}  />`;
  menuIcons['agenda'] = `<img src=${pampaAgendaIcon}  />`;
  menuIcons['descargas'] = `<img src=${pampaDescargasIcon}  />`;
  menuIcons['correspondencia'] = `<img src=${pampaCorrespondenciaIcon}  />`;
  menuIcons['expensas'] = `<img src=${pampaExpensasIcon}  />`;
  menuIcons['infracciones'] = `<img src=${pampaInfraccionIcon}  />`;
  menuIcons['comisiones'] = `<img src=${pampaClasificadosIcon}  />`;
}

export default menuIcons;