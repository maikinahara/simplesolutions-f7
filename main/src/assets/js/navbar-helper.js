// import Helper from './helper'
import store from '../store'
export const NavbarHelper = {
  setNavbarFilterIcon: page => {
    switch (page) {
      case 'guiabarrial':
        // store.dispatch('setFilterIconData', {
        //   show: true,
        //   color: '#fff',
        //   selector: '.smart-select-guiabarrial'
        // })
        store.dispatch('setShowInfo', {show: true, icon: 'filter_list', isSale: false, isGuiaBarrial: true} )
        break
      default:
        store.dispatch('setShowInfo', {show: false, icon: 'filter_list', isSale: false, isGuiaBarrial: false} )
        break
    }
  }
}

