export default [
  {
    path: '/',
    component: require('../pages/login.vue')
  },
  {
    path: '/home',
    component: require('../pages/home.vue')
  },
  {
    path: '/about/',
    component: require('../pages/about.vue')
  }
]
