import store from './../store';
import Helper from './helper';

function post(url, idAccion, idPantalla, callback, dataType = 'json') {
  let params = {
    url: url,
    dataType: dataType,
    callback: callback,
    data: {
      idPantalla: idPantalla,
      idAccion: idAccion,
      idBarrio: store.getters.credentials.barrio.id,
      idUsuario: store.getters.credentials.user,
      idPassword: store.getters.credentials.password
    }
  };
  Helper.post(params);
}

export function postInicio(idPantalla, callback, dataType) {
  post(Helper.URL.GetMain, 'Inicio', idPantalla, callback, dataType)
}

export function postCoordenadas(callback) {
  post(Helper.URL.GetMain, 'Coordenadas', 1, callback)
}