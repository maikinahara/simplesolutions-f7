import Helper from './helper'
import store from '../store'
export const ToolbarHelper = {
  standarSelectAll: (data) => {
    store.dispatch('toggleSelectAllShow', data)
  },
  standarToolbar: () => {
    store.dispatch('setToolbarConfig', {
      isAdmin: false,
      text: '',
      title: '',
      previousTitle: '',
      showToolbar: true,
      showGirovisionButton: false,
      event: ''
    })
  },
  standarFab: () => {
    store.dispatch('setFabConfig', {
      show: false,
      event: ''
    })
  },
  standarQueryPrivateNote: () => {
    store.dispatch('setQueryPrivateNote', {
      show: false
    })
  },
  setToolbar: page => {
    let show = Helper.ShowSelectAllMenu(page)
    ToolbarHelper.standarSelectAll({show: show, button: 'WP'})
    ToolbarHelper.standarToolbar()
    ToolbarHelper.standarFab()
    ToolbarHelper.standarQueryPrivateNote()
    switch (page) {
      case 'agenda':
        ToolbarHelper.standarToolbar()
        break
      case 'boletas':
        ToolbarHelper.standarToolbar()
        break
      case 'clasificados':
        store.dispatch('setToolbarConfig', {
          isAdmin: true,
          text: 'Mis Avisos',
          title: '',
          previousTitle: '',
          showToolbar: true,
          showGirovisionButton: false,
          event: 'mySalesEvt'
        })
        break
      case 'confirmarReserva':
        ToolbarHelper.standarToolbar()
        break
      case 'consultas':
        store.dispatch('setToolbarConfig', {
          isAdmin: true,
          text: 'Nueva consulta',
          title: 'Nueva consulta',
          previousTitle: 'Consultas',
          showToolbar: true,
          showGirovisionButton: false,
          event: 'newQueryEvt'
        })
        break
      case 'consultasadmin':
        store.dispatch('setToolbarConfig', {
          isAdmin: false,
          text: 'Nueva consulta',
          title: 'Nueva consulta',
          previousTitle: 'Consultas',
          showToolbar: true,
          showGirovisionButton: false,
          event: 'newQueryEvtAdmin'
        })
        store.dispatch('setFabConfig', {
          show: true,
          event: 'newQueryEvtAdmin'
        })
        store.dispatch('toggleSelectAllShow', {show: true, button: 'share'})
        break
      case 'correspondencia':
        ToolbarHelper.standarToolbar()
        break
      case 'descargas':
        ToolbarHelper.standarToolbar()
        // store.dispatch('setToolbarConfig', {
        //   isAdmin: store.getters.credentials.admin,
        //   text: 'Redactar Documento',
        //   title: 'Paso 1: Redactar',
        //   previousTitle: '',
        //   showToolbar: true,
        //   showGirovisionButton: false,
        //   event: 'btnNewDocumentEvt'
        // })
        break
      case 'documentosNivel3':
        ToolbarHelper.standarToolbar()
        break
      case 'encuestas':
        ToolbarHelper.standarToolbar()
        break
      case 'expensas':
        store.dispatch('setToolbarConfig', {
          isAdmin: store.getters.credentials.admin,
          text: 'Redactar Expensa',
          title: 'Paso 1: Redactar',
          previousTitle: 'Expensas',
          showToolbar: true,
          showGirovisionButton: false,
          event: ''
        })
        break
      case 'foros':
        ToolbarHelper.standarToolbar()
        break
      case 'guiabarrial':
        store.dispatch('setToolbarConfig', {
          isAdmin: true,
          text: 'Sugerir',
          // title: 'Paso 1: Redactar',
          previousTitle: 'Guia Barrial',
          showGirovisionButton: false,
          showToolbar: true,
          event: 'suggestProduct'
        })
        break
      case 'informepago':
        ToolbarHelper.standarToolbar()
        break
      case 'infracciones':
        ToolbarHelper.standarToolbar()
        store.dispatch('setFabConfig', {
          show: store.getters.credentials.admin,
          event: 'newInfractionEvt'
        })
        break
      case 'newInfraction':
        ToolbarHelper.standarToolbar()
        ToolbarHelper.standarFab()
        break
      case 'invitados':
        store.dispatch('setToolbarConfig', {
          isAdmin: true,
          text: 'Nuevo Invitado',
          title: '',
          icon: 'people',
          previousTitle: 'Invitados',
          showToolbar: true,
          showGirovisionButton: true,
          event: 'newGuestEvt',
          eventGiro: 'inviteGuestEvt'
        })
        // store.dispatch('setToolbarConfig', {
        //   isAdmin: true,
        //   text: 'Nuevo Invitado',
        //   title: '',
        //   icon: 'people',
        //   previousTitle: 'Invitados',
        //   showToolbar: true,
        //   event: 'newGuestEvt'
        // })
        break
      case 'invitadosingesys':
        store.dispatch('setToolbarConfig', {
          isAdmin: true,
          text: 'Nuevo Invitado',
          title: '',
          icon: 'people',
          previousTitle: 'Invitados',
          showToolbar: true,
          event: 'newGuestIngesysEvt'
        })
        break
      case 'invitadosgirovision':
        store.dispatch('setToolbarConfig', {
          isAdmin: true,
          text: 'Cargar nuevo invitado',
          title: '',
          icon: 'none',
          previousTitle: 'Invitados',
          showToolbar: true,
          showGirovisionButton: true,
          event: 'newGuestGirovisionEvt',
          eventGiro: 'inviteGirovisionEvt'
        })
        break
      case 'mostrarConsultas':
        store.dispatch('setToolbarConfig', {
          isAdmin: true,
          text: 'Responder',
          title: 'Nueva consulta',
          previousTitle: '',
          showToolbar: true,
          showGirovisionButton: false,
          event: 'newQuery2Evt',
        })
        ToolbarHelper.standarQueryPrivateNote()
        break
      case 'mostrarConsultasAdmin':
        store.dispatch('setToolbarConfig', {
          isAdmin: false,
          showQueryDataMenu: true,
          text: 'Responder',
          title: 'Nueva consulta',
          previousTitle: '',
          showToolbar: true,
          showGirovisionButton: false,
          event: 'newQuery2EvtAdmin',
        })
        store.dispatch('setQueryPrivateNote', {
          show: true,
        })
        ToolbarHelper.standarFab()
        break
      case 'mostrarNovedad':
        ToolbarHelper.standarToolbar()
        break
      case 'mostrarExpensa':
        ToolbarHelper.standarToolbar()
        break
      case 'mostrarDescarga':
        ToolbarHelper.standarToolbar()
        break
      case 'mostrarDocumento':
        ToolbarHelper.standarToolbar()
        break
      case 'novedades':
        // var asd = store
        store.dispatch('setToolbarConfig', {
          isAdmin: store.getters.credentials.admin,
          text: 'Redactar Novedad',
          title: 'Paso 1: Redactar',
          previousTitle: 'Novedades',
          showToolbar: true,
          showGirovisionButton: false,
          event: 'newNewsEvt',
        })
        break
      case 'novedadescastores':
        // var asd = store
        store.dispatch('setToolbarConfig', {
          isAdmin: store.getters.credentials.admin,
          text: 'Redactar Novedad',
          title: 'Paso 1: Redactar',
          previousTitle: 'Novedades',
          showToolbar: true,
          showGirovisionButton: false,
          event: 'newNewsCastoresEvt',
        })
        break
      case 'showMySales':
        store.dispatch('setToolbarConfig', {
          isAdmin: true,
          text: 'Crear aviso',
          title: '',
          previousTitle: '',
          showToolbar: true,
          showGirovisionButton: false,
          event: 'newSaleEvt'
        })
        break
      case 'showMySale':
        ToolbarHelper.standarToolbar()
        break
      case 'myReservations':
        ToolbarHelper.standarToolbar()
        break
      case 'pago':
        ToolbarHelper.standarToolbar()
        break
      case 'pagosInformados':
        ToolbarHelper.standarToolbar()
        break
      case 'reservaDetalle':
        ToolbarHelper.standarToolbar()
        break
      case 'reservas':
        store.dispatch('setToolbarConfig', {
          isAdmin: true,
          text: 'Mis Reservas',
          title: 'Mis Reservas',
          previousTitle: 'Reservas',
          showToolbar: true,
          showGirovisionButton: false,
          icon: 'date_range',
          event: 'showReservationsEvt'
        })
        break
      case 'sendGirovisionInvite':
        ToolbarHelper.standarToolbar()
        break
      case 'telefonos':
        ToolbarHelper.standarToolbar()
        break
      default:
        store.dispatch('setToolbarConfig', {
          isAdmin: store.getters.credentials.admin,
          text: '',
          title: '',
          previousTitle: '',
          showToolbar: false,
          showGirovisionButton: false,
          event: ''
        })
        ToolbarHelper.standarFab()
        break
    }
  }
}