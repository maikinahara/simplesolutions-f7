const PW = {}

PW.android = {
  init: () => {
    registerPushwooshAndroid()
  }
}

PW.iOS = {
  init: () => {
    registerPushwooshIOS()
  }
}

PW.windowsPhone = {
  init: () => {
    registerPushwooshWP()
  }
}

let model = {
  android: {
    projectid: '',
    appid: ''
  },
  ios: {
    pw_appid: ''
  },
  wp: {
    appid: '',
    serviceName: ''
  }
}

switch (window.APPTORUN) {
  case 'ss':
    model.android.projectid = '38608996272'
    model.android.appid = '634E1-E34E3'
    model.ios.pw_appid = '634E1-E34E3'
    model.wp.appid = '634E1-E34E3'
    model.wp.serviceName = ''
    break
  case 'administrar':
    model.android.projectid = '601948976295'
    model.android.appid = 'CE0E4-77D54'
    model.ios.pw_appid = 'CE0E4-77D54'
    model.wp.appid = 'CE0E4-77D54'
    model.wp.serviceName = ''
    break;
  case 'arboris':
    model.android.projectid = '551763459157'
    model.android.appid = '58D68-D2F7E'
    model.ios.pw_appid = '58D68-D2F7E'
    model.wp.appid = '58D68-D2F7E'
    model.wp.serviceName = ''
    break;
  case 'ayres':
    model.android.projectid = '1051389191971'
    model.android.appid = '7884A-33667'
    model.ios.pw_appid = '7884A-33667'
    model.wp.appid = '7884A-33667'
    model.wp.serviceName = ''
    break;
  case 'gabsa':
    model.android.projectid = '716282698910'
    model.android.appid = 'D9686-7F4FD'
    model.ios.pw_appid = 'D9686-7F4FD'
    model.wp.appid = 'D9686-7F4FD'
    model.wp.serviceName = ''
    break;
  case 'laguna':
    model.android.projectid = '596266296713'
    model.android.appid = 'DD6DA-74D8B'
    model.ios.pw_appid = 'DD6DA-74D8B'
    model.wp.appid = 'DD6DA-74D8B'
    model.wp.serviceName = ''
    break;
  case 'martindale':
    model.android.projectid = '646030061859'
    model.android.appid = 'AD823-4C690'
    model.ios.pw_appid = 'AD823-4C690'
    model.wp.appid = 'AD823-4C690'
    model.wp.serviceName = ''
    break;
  case 'mayling':
    model.android.projectid = '833082856093'
    model.android.appid = 'A19C7-E3138'
    model.ios.pw_appid = 'A19C7-E3138'
    model.wp.appid = 'A19C7-E3138'
    model.wp.serviceName = ''
    break;
  case 'vergara':
    model.android.projectid = '26746362387'
    model.android.appid = '6D1D8-D1C51'
    model.ios.pw_appid = '6D1D8-D1C51'
    model.wp.appid = '6D1D8-D1C51'
    model.wp.serviceName = ''
    break;
  case 'villanueva':
    model.android.projectid = '235270193142'
    model.android.appid = '7BE19-FD74A'
    model.ios.pw_appid = '7BE19-FD74A'
    model.wp.appid = '7BE19-FD74A'
    model.wp.serviceName = ''
    break;
  case 'lagartos':
    model.android.projectid = '199757895919'
    model.android.appid = 'D095E-E73C9'
    model.ios.pw_appid = 'D095E-E73C9'
    model.wp.appid = 'D095E-E73C9'
    model.wp.serviceName = ''
    break;
  case 'dibartolo':
    model.android.projectid = '562861989381'
    model.android.appid = 'DAC4D-B2457'
    model.ios.pw_appid = 'DAC4D-B2457'
    model.wp.appid = 'DAC4D-B2457'
    model.wp.serviceName = ''
    break;
  case 'carmel':
    model.android.projectid = '666795250399'
    model.android.appid = 'F62AB-69050'
    model.ios.pw_appid = 'F62AB-69050'
    model.wp.appid = 'F62AB-69050'
    model.wp.serviceName = ''
    break;
  case 'castores':
    model.android.projectid = '760410497656'
    model.android.appid = '33D9C-2728B'
    model.ios.pw_appid = '33D9C-2728B'
    model.wp.appid = '33D9C-2728B'
    model.wp.serviceName = ''
    break;
  case 'providencia':
    model.android.projectid = '438162422282'
    model.android.appid = 'B2EF1-C3DC2'
    model.ios.pw_appid = 'B2EF1-C3DC2'
    model.wp.appid = 'B2EF1-C3DC2'
    model.wp.serviceName = ''
    break;
  case 'pampa':
    model.android.projectid = '1068641443326'
    model.android.appid = '4CBD2-7A957'
    model.ios.pw_appid = '4CBD2-7A957'
    model.wp.appid = '4CBD2-7A957'
    model.wp.serviceName = ''
    break;
  case 'admsanjuan':
    model.android.projectid = '648793081901'
    model.android.appid = 'DF6AD-51524'
    model.ios.pw_appid = 'DF6AD-51524'
    model.wp.appid = 'DF6AD-51524'
    model.wp.serviceName = ''
    break;
  default:
    console.log('Error setting pushwoosh')
    break;
}

function registerPushwooshAndroid() {
  let pushNotification = window.plugins.pushNotification
  document.addEventListener('push-notification', event => {
    // alert('event.notification: ' + JSON.stringify(event.notification, null, 4))
    let title = event.notification.android.title;
    let userData = event.notification.userdata;
    if (typeof(userData) != 'undefined') {
      console.warn('user data: ' + JSON.stringify(userData))
    }
    // alert('new push ' + JSON.stringify(event.notification, null, 4))
    console.warn(title)
  })
  pushNotification.onDeviceReady ({ projectid: model.android.projectid, appid: model.android.appid })
  pushNotification.registerDevice( token => {
    // console.log('tkn: ' + JSON.stringify(token, null, 4))
      onPushwooshAndroidInitialized(token)
  }, status => {
      console.warn(JSON.stringify(['failed to register ', status]))
  })
}

function onPushwooshAndroidInitialized(pushToken) {
  // alert('push token: ' + pushToken);
  let pushNotification = window.plugins.pushNotification
  pushNotification.setTags(getTags(), status => {
    // console.info('setTags success ' + JSON.stringify(status, null, 4))
  }, status => {
    console.warn('setTags failed');
  });
  pushNotification.setLightScreenOnNotification(false)
}

function registerPushwooshIOS() {
  let pushNotification = window.plugins.pushNotification
  document.addEventListener('push-notification', event => {
    let notification = event.notification
    pushNotification.setApplicationIconBadgeNumber(0)
  })
  pushNotification.onDeviceReady({ pw_appid: model.ios.pw_appid })
  pushNotification.registerDevice( status => {
    let deviceToken = status['deviceToken']
    console.warn('registerDevice: ' + deviceToken)
    onPushwooshiOSInitialized(deviceToken)
  }, status => {
    console.warn('failed to register : ' + JSON.stringify(status))
  })
  pushNotification.setApplicationIconBadgeNumber(0)
}

function onPushwooshiOSInitialized(pushToken) {
  let pushNotification = window.plugins.pushNotification;
  pushNotification.setTags(getTags(), status => {
    console.warn('setTags success')
  },
  status => {
    console.warn('setTags failed')
  })
}

function registerPushwooshWP() {
  let pushNotification = window.plugins.pushNotification
  document.addEventListener('push-notification', event => {
  let notification = event.notification
  })
  pushNotification.onDeviceReady({ appid: model.wp.appid, serviceName: model.wp.serviceName })
  pushNotification.registerDevice( status => {
    let deviceToken = status
    console.warn('registerDevice: ' + deviceToken);
    onPushwooshWPInitialized()
  }, status => {
    console.warn('failed to register : ' + JSON.stringify(status));
  })
}

function onPushwooshWPInitialized() {
  let pushNotification = window.plugins.pushNotification;
  pushNotification.setTags(getTags(), status => {
      console.warn('setTags success');
    }, status => {
      console.warn('setTags failed')
  })
}

function getTags() {
  let enabled = window.localStorage.getItem("Notifications_Enabled_SS");
  if (!enabled || enabled == null || enabled == 'null' || enabled == 'undefined' || enabled == undefined) {
    window.localStorage.setItem('Notifications_Enabled_SS', 'YES');
  }
  try {
    let usuario, barrio, notificaciones
    usuario = window.localStorage.getItem('usuario').toLowerCase();
    barrio = window.localStorage.getItem('barrio')
    notificaciones = window.localStorage.getItem('Notifications_Enabled_SS');
    // alert('tags: ' + JSON.stringify({
    //     "Usuario": usuario,
    //     "Barrio": barrio,
    //     "Notifications_Enabled": notificaciones
    // }, null, 4))
    return {'Usuario': usuario, 'Barrio': barrio, 'Notifications_Enabled': notificaciones}
  } catch (e) {
    console.log(e)
    alert('WP getTags error: ' + e)
    return {'Usuario': '', 'Barrio': '', 'Notifications_Enabled': ''}
  }
}

export default PW