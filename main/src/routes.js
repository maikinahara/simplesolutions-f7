export default [
  {
    path: '/test/',
    component: require('./pages/test.vue')
  },
  {
    path: '/login-ss/',
    component: require('./pages/login/login-ss.vue')
  },
  {
    path: '/registro-ss/',
    component: require('./pages/register/registro-ss.vue')
  },
  {
    path: '/login-ayres/',
    component: require('./pages/login/login-ayres.vue')
  },
  {
    path: '/login-arboris/',
    component: require('./pages/login/login-arboris.vue')
  },
  {
    path: '/registro-arboris/',
    component: require('./pages/register/registro-arboris.vue')
  },
  {
    path: '/registro-laguna/',
    component: require('./pages/register/registro-laguna.vue')
  },
  {
    path: '/recuperar-clave-ss/',
    component: require('./pages/recover-password/recuperar-clave-ss.vue')
  },
  {
    path: '/recuperar-clave-arboris/',
    component: require('./pages/recover-password/recuperar-clave-arboris.vue')
  },
  {
    path: '/recuperar-clave-laguna/',
    component: require('./pages/recover-password/recuperar-clave-laguna.vue')
  },
  {
    path: '/login-laguna/',
    component: require('./pages/login/login-laguna.vue')
  },
  {
    path: '/login-gabsa/',
    component: require('./pages/login/login-gabsa.vue')
  },
  {
    path: '/registro-gabsa/',
    component: require('./pages/register/registro-gabsa.vue')
  },
  {
    path: '/recuperar-clave-gabsa/',
    component: require('./pages/recover-password/recuperar-clave-gabsa.vue')
  },
  {
    path: '/login-villanueva/',
    component: require('./pages/login/login-villanueva.vue')
  },
  {
    path: '/registro-villanueva/',
    component: require('./pages/register/registro-villanueva.vue')
  },
  {
    path: '/recuperar-clave-villanueva/',
    component: require('./pages/recover-password/recuperar-clave-villanueva.vue')
  },
  {
    path: '/login-mayling/',
    component: require('./pages/login/login-mayling.vue')
  },
  {
    path: '/registro-mayling/',
    component: require('./pages/register/registro-mayling.vue')
  },
  {
    path: '/recuperar-clave-mayling/',
    component: require('./pages/recover-password/recuperar-clave-mayling.vue')
  },
  {
    path: '/login-administrar/',
    component: require('./pages/login/login-administrar.vue')
  },
  {
    path: '/registro-ayres/',
    component: require('./pages/register/registro-ayres.vue')
  },
  {
    path: '/registro-administrar/',
    component: require('./pages/register/registro-administrar.vue')
  },
  {
    path: '/recuperar-clave-administrar/',
    component: require('./pages/recover-password/recuperar-clave-administrar.vue')
  },
  {
    path: '/login-martindale/',
    component: require('./pages/login/login-martindale.vue')
  },
  {
    path: '/registro-martindale/',
    component: require('./pages/register/registro-martindale.vue')
  },
  {
    path: '/recuperar-clave-martindale/',
    component: require('./pages/recover-password/recuperar-clave-martindale.vue')
  },
  {
    path: '/login-vergara/',
    component: require('./pages/login/login-vergara.vue')
  },
  {
    path: '/registro-vergara/',
    component: require('./pages/register/registro-vergara.vue')
  },
  {
    path: '/recuperar-clave-vergara/',
    component: require('./pages/recover-password/recuperar-clave-vergara.vue')
  },
  {
    path: '/login-dibartolo/',
    component: require('./pages/login/login-dibartolo.vue')
  },
  {
    path: '/registro-dibartolo/',
    component: require('./pages/register/registro-dibartolo.vue')
  },
  {
    path: '/recuperar-clave-dibartolo/',
    component: require('./pages/recover-password/recuperar-clave-dibartolo.vue')
  },
  {
    path: '/login-carmel/',
    component: require('./pages/login/login-carmel.vue')
  },
  {
    path: '/registro-carmel/',
    component: require('./pages/register/registro-carmel.vue')
  },
  {
    path: '/recuperar-clave-carmel/',
    component: require('./pages/recover-password/recuperar-clave-carmel.vue')
  },
  {
    path: '/login-castores/',
    component: require('./pages/login/login-castores.vue')
  },
  {
    path: '/registro-castores/',
    component: require('./pages/register/registro-castores.vue')
  },
  {
    path: '/recuperar-clave-castores/',
    component: require('./pages/recover-password/recuperar-clave-castores.vue')
  },
  {
    path: '/login-lagartos/',
    component: require('./pages/login/login-lagartos.vue')
  },
  {
    path: '/registro-lagartos/',
    component: require('./pages/register/registro-lagartos.vue')
  },
  {
    path: '/recuperar-clave-lagartos/',
    component: require('./pages/recover-password/recuperar-clave-lagartos.vue')
  },
  {
    path: '/login-espartanos/',
    component: require('./pages/login/login-espartanos.vue')
  },
  {
    path: '/registro-espartanos/',
    component: require('./pages/register/registro-espartanos.vue')
  },
  {
    path: '/recuperar-clave-espartanos/',
    component: require('./pages/recover-password/recuperar-clave-espartanos.vue')
  },
  {
    path: '/login-providencia/',
    component: require('./pages/login/login-providencia.vue')
  },
  {
    path: '/registro-providencia/',
    component: require('./pages/register/registro-providencia.vue')
  },
  {
    path: '/recuperar-clave-providencia/',
    component: require('./pages/recover-password/recuperar-clave-providencia.vue')
  },
  {
    path: '/login-pampa/',
    component: require('./pages/login/login-pampa.vue')
  },
  {
    path: '/registro-pampa/',
    component: require('./pages/register/registro-pampa.vue')
  },
  {
    path: '/recuperar-clave-pampa/',
    component: require('./pages/recover-password/recuperar-clave-pampa.vue')
  },
  {
    path: '/login-admsanjuan/',
    component: require('./pages/login/login-admsanjuan.vue')
  },
  {
    path: '/registro-admsanjuan/',
    component: require('./pages/register/registro-admsanjuan.vue')
  },
  {
    path: '/recuperar-clave-admsanjuan/',
    component: require('./pages/recover-password/recuperar-clave-admsanjuan.vue')
  },
  {
    path: '/home/',
    component: require('./pages/home.vue')
  },
  {
    path: '/prehome-ayres/',
    component: require('./pages/prehome-ayres.vue')
  },
  {
    path: '/novedades-ayres/',
    component: require('./pages/novedades-ayres.vue')
  },
  {
    path: '/comunidad-ayres/',
    component: require('./pages/comunidad-ayres.vue')
  },
  {
    path: '/recuperar-clave-ayres/',
    component: require('./pages/recover-password/recuperar-clave-ayres.vue')
  },
  {
    path: '/novedades/',
    component: require('./pages/novedades.vue')
  },
  {
    path: '/novedadescastores/',
    component: require('./pages/novedadescastores.vue')
  },
  {
    path: '/mostrarNovedad/',
    component: require('./pages/mostrarNovedad.vue')
  },
  {
    path: '/show-news-ayres/',
    component: require('./pages/show-news-ayres.vue')
  },
  {
    path: '/expensas/',
    component: require('./pages/expensas.vue')
  },
  {
    path: '/mostrarExpensa',
    component: require('./pages/mostrarExpensa.vue')
  },
  {
    path: '/descargas/',
    component: require('./pages/descargas.vue')
  },
  {
    path: '/mostrarDescarga/',
    component: require('./pages/mostrarDescarga.vue')
  },
  {
    path: '/documentosNivel3/',
    component: require('./pages/documentosNivel3.vue')
  },
  {
    path: '/mostrarDocumento/:documentId/:documentTitle/:documentDate/',
    component: require('./pages/mostrarDocumento.vue')
  },
  {
    path: '/mostrarDocumentosNivel3/:documentId/:documentTitle/:documentDate/',
    component: require('./pages/mostrarDocumento.vue')
  },
  {
    path: '/redactarNovedades/',
    component: require('./pages/redactarNovedades.vue')
  },
  {
    path: '/publicarNovedades/:title/:text/:img/',
    component: require('./pages/publicarNovedades.vue')
  },
  {
    path: '/profile/',
    component: require('./pages/profile.vue')
  },
  {
    path: '/asistencia-tecnica/',
    component: require('./pages/asistencia-tecnica.vue')
  },
  {
    path: '/encuestas/',
    component: require('./pages/encuestas.vue')
  },
  {
    path: '/mostrarEncuesta/:idPoll/:isActive/',
    component: require('./pages/mostrarEncuesta.vue')
  },
  {
    path: '/camaras/',
    component: require('./pages/camaras.vue')
  },
  {
    path: '/consultas/',
    component: require('./pages/consultas.vue')
  },
  {
    path: '/consultasadmin/',
    component: require('./pages/consultas-admin.vue')
  },
  {
    path: '/mostrarConsultas',
    component: require('./pages/mostrarConsultas.vue')
  },
  {
    path: '/mostrarConsultasAdmin',
    component: require('./pages/mostrarConsultasAdmin.vue')
  },
  {
    path: '/nuevaConsulta/:idQuery/:queryType/',
    component: require('./pages/nuevaConsulta.vue')
  },
  {
    path: '/nuevaConsultaAdmin/:idQuery/:queryType/',
    component: require('./pages/nuevaConsultaAdmin.vue')
  },
  {
    path: '/reservas/',
    component: require('./pages/reservas.vue')
  },
  {
    path: '/myReservations/',
    component: require('./pages/myReservations.vue')
  },
  {
    path: '/showReservation/',
    component: require('./pages/showReservation.vue')
  },
  // {
  //   path: '/reservaDetalle/:idReservation/',
  //   component: require('./pages/reservaDetalle.vue')
  // },
  {
    path: '/reservaDetalle/',
    component: require('./pages/reservaDetalle.vue')
  },
  {
    path: '/confirmarReserva/',
    component: require('./pages/confirmarReserva.vue')
  },
  {
    path: '/informepago/',
    component: require('./pages/informepago.vue')
  },
  {
    path: '/confirmarInformePago/',
    component: require('./pages/confirmarInformePago.vue')
  },
  {
    path: '/pagosInformados/',
    component: require('./pages/pagosInformados.vue')
  },
  {
    path: '/pago/:idPayment',
    component: require('./pages/pago.vue')
  },
  {
    path: '/telefonos/',
    component: require('./pages/telefonos.vue')
  },
  {
    path: '/invitados/',
    component: require('./pages/invitados.vue')
  },
  {
    path: '/showGuest/:idGuest/',
    component: require('./pages/showGuest.vue')
  },
  {
    path: '/confirmGuest/',
    component: require('./pages/confirmGuest.vue')
  },
  {
    path: '/invitadosgirovision/',
    component: require('./pages/invitadosgirovision.vue')
  },
  {
    path: '/showGuestGirovision/:idGuest/',
    component: require('./pages/showGuestGirovision.vue')
  },
  {
    path: '/sendGuestInvite',
    component: require('./pages/sendGuestInvite.vue')
  },
  {
    path: '/sendGirovisionInvite',
    component: require('./pages/sendGirovisionInvite.vue')
  },
  {
    path: '/confirmGuestGirovision/',
    component: require('./pages/confirmGuestGirovision.vue')
  },
  {
    path: '/agenda/',
    component: require('./pages/agenda.vue')
  },
  {
    path: '/comisiones/',
    component: require('./pages/commissions.vue')
  },
  {
    path: '/showDiary/',
    component: require('./pages/showDiary.vue')
  },
  {
    path: '/infracciones/',
    component: require('./pages/infracciones.vue')
  },
  {
    path: '/showInfraction/',
    component: require('./pages/showInfraction.vue')
  },
  {
    path: '/newInfraction/',
    component: require('./pages/newInfraction.vue')
  },
  {
    path: '/claimInfraction/',
    component: require('./pages/claimInfraction.vue')
  },
  {
    path: '/boletas/',
    component: require('./pages/boletas.vue')
  },
  {
    path: '/showBallot/',
    component: require('./pages/showBallot.vue')
  },
  {
    path: '/foros/',
    component: require('./pages/foros.vue')
  },
  {
    path: '/showForum/',
    component: require('./pages/showForum.vue')
  },
  {
    path: '/showTopic/',
    component: require('./pages/showTopic.vue')
  },
  {
    path: '/newTopic/',
    component: require('./pages/newTopic.vue')
  },
  {
    path: '/newCategory/',
    component: require('./pages/newCategory.vue')
  },
  {
    path: '/clasificados/',
    component: require('./pages/clasificados.vue')
  },
  {
    path: '/showSale/',
    component: require('./pages/showSale.vue')
  },
  {
    path: '/showSaleAyres/',
    component: require('./pages/showSaleAyres.vue')
  },
  {
    path: '/showMySales/',
    component: require('./pages/showMySales.vue')
  },
  {
    path: '/showMySale/:type',
    component: require('./pages/showMySale.vue')
  },
  {
    path: '/correspondencia',
    component: require('./pages/correspondencia.vue')
  },
  {
    path: '/showMail',
    component: require('./pages/showMail.vue')
  },
  {
    path: '/invitadosingesys',
    component: require('./pages/invitadosingesys.vue')
  },
  {
    path: '/newIngesysGuest',
    component: require('./pages/newIngesysGuest.vue')
  },
  {
    path: '/showIngesysGuest',
    component: require('./pages/showIngesysGuest.vue')
  },
  {
    path: '/guiabarrial',
    component: require('./pages/guiabarrial.vue')
  },
  {
    path: '/suggestProduct',
    component: require('./pages/suggestProduct.vue')
  }
]
